#!/usr/bin/env ruby

=begin
This script will get a list of all WordPress databases from the Construct-DB and create a new database
that will be used to map the hashed name of the database to the domain name of the site that use it.
This is required by the flattening script and others to easily query the Construct-DB for information
about a given domain
=end

require 'mysql2'
require 'logger'


module Construct_db_domain_map
  class Dbs2Domains
    def initialize(args, log)
      database_ip = args[:database_ip]
      database_user = args[:database_user]
      database_password = args[:database_password]
      @log  = log

      begin
        @client = Mysql2::Client.new(:host => database_ip, :username => database_user, :password => database_password)
      rescue => e
        @log.error(e)
        exit(1)
      end
    end


    def close
      @client.close if @client
    end


    def walk_the_hash
      db2site_h   = {}
      @client.query('show databases', :symbolize_keys => true).each do |row|
        begin
          row.values.each do |database|
            value_returned = self.get_domain_name(database)
            if value_returned == nil
              @log.info("'nil' was returned from get_domain_name() for: #{database}")
              next
            else
              @log.info("adding #{database} to db2site_h")
              db2site_h[database] = value_returned
            end
          end
        rescue => e
          puts e
          next
        end
      end
      db2site_h
    end


    def get_domain_name(database)
      abort('get_domain_name expects a String input!') unless database.kind_of?(String)
      begin
        @client.query("use #{database};")
        result = @client.query("select option_value from wp_options where option_name = 'siteurl';")
        result.each do |row|
          value = (row['option_value'].split('/')[2]).split('.')[1,2].join('.')
          return value
        end
      rescue => e
        puts e
        return nil
      end
    end
  end


  class Domains2Dbs
    def initialize(args, log)
      database_name = args[:database_name]
      database_ip = args[:database_ip]
      database_user = args[:database_user]
      database_password = args[:database_password]

      @log = log

      begin
        @client = Mysql2::Client.new(:host => database_ip, :username => database_user, :password => database_password,  :database => database_name)
      rescue => e
        @log.info("Domains2DBs initialization Error!!! = #{e}")
        exit(1)
      end
    end


    def close
      @client.close
    end


    def walk_the_hash(records_h)
      records_h.each do |database_name, domain_name|
        begin
          insert_status = self.insert_records(database_name, domain_name)
          insert_status ? next : @log.info(insert_status.inspect)
        rescue => e
          puts e
          @log.info(e)
          next
        end
      end
    end


    def insert_records(database_name, domain_name)
      puts "insert_records: #{database_name}, #{domain_name}"
      begin
        ret = @client.query("INSERT INTO domains (domain_name, database_name) VALUES (\"#{domain_name}\", \"#{database_name}\");")
        puts ret.inspect
        return true
      rescue => e
        puts e
        return false
      end
    end
  end # Class end


  if __FILE__ == $0
    begin
      #result = { :successful => true }
      records_h = {}
      args = {
      :database_ip => '10.0.1.92',
      :database_name => 'domainNames2dbNames',
      :database_user => 'root',
      :database_password => 'zwt4ofEaKlhHT2E'
      }

      # setup global logging
      log = Logger.new('db2domain.log', 'daily')
      log.datetime_format = "%Y-%m-%d %H:%M:%S"

      # get the list of all WordPress databases on Construct-DB
      db2dom = Dbs2Domains.new(args, log)
      records_h = db2dom.walk_the_hash
      db2dom.close

      # create a row for each domain in the 'domainmap' database
      dom2db = Domains2Dbs.new(args, log)
      dom2db.walk_the_hash(records_h)
      dom2db.close
    rescue => e
      puts "Stack Trace: #{e}"
    ensure
      db2dom.close if db2dom
      dom2db.close if dom2db
    end
  end
end

